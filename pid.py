#!/bin/python

from time import sleep

width = 190#300

start = 60
goal = 170

velocity = 0
friction = 0
a_step = 0.1

Kp = 0.05
Ki = 0
Kd = 0.8

cutoff = 30

fill = " "
of = open("PID_OUTPUT.txt", "w")
buff = ""

def display(location):
    #global width, start, goal, location, velocity, friction, a_step, Kp, Ki, Kd, cutoff, t, fill
    out = ""
    for x in range(width-1):
        if x == int(location):
            out += "+"
        elif x == goal:
            out += "*"
        else:
            out += fill

    print(out, end="\r")

def out(location):
    global width, goal, of, fill, buff
    for x in range(width-1):
        if x == int(location):
            buff += "+"
        elif x == goal:
            buff += "|"
        else:
            buff += fill
    buff += "\n"

def main():
    global width, start, goal, friction, a_step, cutoff, fill, of, buff
    Ki = 0.00
    n = 500
    for x in range(1, n):
        Kp = x / n
        print("now working on Kp = " + str(Kp))
        for y in range(1, n):
            Kd = y / n

            location = start
            velocity = 0
            acceleration = 0
            integral = 0
            prev_err = goal - location
            result = 500
            t = 0
            buff += "Kp: " + str(Kp) + "\tKd: " + str(Kd) + "-"*20 + "\n"
            MAX_T = 90
            while abs(result) > 0.00001 and t < MAX_T:
                t += 1
                # PID stuff here
                error = goal - location
                integral += error
                derivative = (error - prev_err) / 1
                result = Kp * error + Ki * integral + Kd * derivative
                prev_err = error
                if result == 0:
                    break
                acceleration = min(abs(result*0.3), 30) * abs(result)/result
#                print(str(result) + "\t" + str(location))
                velocity += acceleration
                location += velocity
                out(location)
                #sleep(0.001)
            if t < MAX_T and abs(location - goal) < 1:
                of.write(buff)
            buff = ""


if __name__ == "__main__":
    main()
    of.close()
