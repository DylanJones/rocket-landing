import logging
import sys

import core

logger = logging.getLogger()
if '--debug' in sys.argv or '-d' in sys.argv:
    logger.setLevel(logging.DEBUG)
    # take care of adafruit logger being bad
    logging.getLogger("Adafruit_I2C.Device.Bus.1.Address.0X1D").setLevel(logging.INFO)
else:
    logger.setLevel(logging.INFO)

if __name__ == '__main__':
    logging.info("Starting application")
    core.startup()
