# Thin wrapper around pFS that exists so that we can run it in another process
import logging
import os


def startup():
    os.chdir("pFS")
    import core

    # Pretend that we're main.py
    logging.getLogger().setLevel(logging.DEBUG)
    core.startup()
