#!/bin/python
from java.lang import Integer
from jep import jproxy


class PyTestInterface:
    def getName():
        return "t"

    def getRandomNumber():
        return Integer(4)


RocketInterface = jproxy(PyTestInterface, ["pw.karel.pyinterface.TestInterface"])
