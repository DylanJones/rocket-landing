import multiprocessing
import time
import jep


class PySimulationListener:
    def __init__(self):
        self.burnt = False

    def startSimulation(self, status):
        # TODO: start pFS
        # print(status.getRocketPosition())
        # self.pfs_process = multiprocessing.Process(target=pfs_wrapper.startup)
        # self.pfs_process.start()
        pass

    def endSimulation(self, status, exception):
        # TODO: stop pFS
        pass

    def preStep(self, status):
        status.setTumbling(False)
        return True

    def postStep(self, status):
        print("Velocity: " + str(status.getRocketVelocity()))
        print("Position: " + str(status.getRocketPosition()))
        # time.sleep(status.getPreviousTimeStep())
        pass

    def isSystemListener(self):
        return False

    def addFlightEvent(self, status, event):
        return True

    def handleFlightEvent(self, status, event):
        return True

    def motorIgnition(self, status, motorId, mount, instance):
        return True

    def recoveryDeviceDeployment(self, status, recoveryDevice):
        return True

    def preAccelerationCalculation(self, status):
        return None

    def preAerodynamicCalculation(self, status):
        return None

    def preAtmosphericModel(self, status):
        return None

    def preFlightConditions(self, status):
        return None

    def preGravityModel(self, status):
        return float('nan')

    def preMassCalculation(self, status):
        return None

    def preSimpleThrustCalculation(self, status):
        return float('nan')

    def preWindModel(self, status):
        return None

    def postAccelerationCalculation(self, status, acceleration):
        return None

    def postAerodynamicCalculation(self, status, forces):
        return None

    def postAtmosphericModel(self, status, atmosphericConditions):
        return None

    def postFlightConditions(self, status, flightConditions):
        return None

    def postGravityModel(self, status, gravity):
        return float('nan')

    def postMassCalculation(self, status, massData):
        return None

    def postSimpleThrustCalculation(self, status, thrust):
        return float('nan')

    def postWindModel(self, status, wind):
        return None


def newInstance():
    return jep.jproxy(PySimulationListener(),
                      ['net.sf.openrocket.simulation.listeners.SimulationComputationListener'])


multiprocessing.set_start_method("spawn")
