package pw.karel.pyinterface;

import net.sf.openrocket.simulation.SimulationConditions;
import net.sf.openrocket.simulation.exception.SimulationException;
import net.sf.openrocket.simulation.extension.AbstractSimulationExtension;
import net.sf.openrocket.simulation.listeners.SimulationListener;

import java.lang.reflect.Proxy;

/**
 * The actual simulation extension.  A new instance is created for
 * each simulation it is attached to.
 * <p>
 * This class contains the configuration and is called before the
 * simulation is run.  It can do changes to the simulation, such
 * as add simulation listeners.
 * <p>
 * All configuration should be stored in the config variable, so that
 * file storage will work automatically.
 */
public class PythonInterface extends AbstractSimulationExtension {

    public PythonInterface() {
        super();
    }

    @Override
    public String getName() {
        return "Python Script: " + getFilename();
    }

    @Override
    public String getDescription() {
        // This description is shown when the user clicks the info-button on the extension
        return "This extension runs a python script and stuff happen.";
    }

    @Override
    public void initialize(SimulationConditions conditions) throws SimulationException {
        // Add the Python simulation module
        SimulationListener listener = (SimulationListener) Proxy.newProxyInstance(
                SimulationListenerInvocationHandler.class.getClassLoader(),
                new Class[]{SimulationListener.class}, new SimulationListenerInvocationHandler());
        conditions.getSimulationListenerList().add(listener);
//        conditions.getSimulationListenerList().add(new TestSimulationListener());
    }

    private String getFilename() {
        return config.getString("filename", "");
    }
}
