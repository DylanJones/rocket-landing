package pw.karel.pyinterface;

import net.sf.openrocket.simulation.SimulationStatus;
import net.sf.openrocket.simulation.listeners.AbstractSimulationListener;
import net.sf.openrocket.simulation.listeners.SimulationComputationListener;

public class TestSimulationListener extends AbstractSimulationListener implements SimulationComputationListener {
    @Override
    public void startSimulation(SimulationStatus status) {
        System.out.println(status);
        status.getPreviousTimeStep();
        status.setTumbling(false);
        status.getRocketVelocity();
        status.getRocketPosition();
    }
}
