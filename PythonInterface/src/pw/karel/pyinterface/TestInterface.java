package pw.karel.pyinterface;

public interface TestInterface {
    public int getRandomNumber();
    public String getName();
}
