package pw.karel.pyinterface;

import jep.Jep;
import net.sf.openrocket.simulation.listeners.SimulationComputationListener;
import net.sf.openrocket.simulation.listeners.SimulationListener;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class functions as the Java wrapper for the Python SimulationListener class.
 * Upon being called, it passes the method on to the main worker thread where it is executed
 * and the results returned.
 */
public class SimulationListenerInvocationHandler implements InvocationHandler {
    // Not sure if this is needed
    private final Lock threadLock = new ReentrantLock();
    // Jep is very particular about threads.  It seems that each SimulationListener is only
    // accessed in a single thread, so it should be fine to keep a reference to the object around.
    private SimulationListener pyListener;

    // Map of Jep interperters to the threads that own them
    private final static HashMap<Long, Jep> threadedJepPool = new HashMap<>();

    SimulationListenerInvocationHandler() {
    }

    // Called whenever OpenRocket tries to call a method
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // Ensure jep exists for this thread
        Jep interpreter;
        if (threadedJepPool.containsKey(Thread.currentThread().getId())) {
//            System.out.println("Thread " + Thread.currentThread().getId() + ": Using existing interpreter for " + this);
            interpreter = threadedJepPool.get(Thread.currentThread().getId());
        } else {
            // An interpreter for this thread doesn't exist yet, so make one
            System.out.println(threadedJepPool);
//            System.out.println("Thread " + Thread.currentThread().getId() + ": Creating interpreter for " + this);
            interpreter = new Jep();
            threadedJepPool.put(Thread.currentThread().getId(), interpreter);
            interpreter.runScript("listener.py");
        }
        // Make sure that we have our instance
        if (pyListener == null) {
            pyListener = (SimulationListener) interpreter.invoke("newInstance");
        }
        threadLock.lock();
        // Execute the method
        try {
            switch (method.getName()) {
                case "clone":
                    System.out.println("Clone invoked");
                    return Proxy.newProxyInstance(SimulationListenerInvocationHandler.class.getClassLoader(),
                            new Class[]{SimulationComputationListener.class}, new SimulationListenerInvocationHandler());
                default:
                    // by default, just pass through the call
                    return method.invoke(pyListener, args);
            }
        } finally {
            threadLock.unlock();
        }
    }
}
