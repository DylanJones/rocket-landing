package pw.karel.pyinterface;

import jep.Jep;
import jep.JepException;

import java.lang.reflect.Proxy;

public class PythonTester {
    public static void main(String... args) throws JepException {
        Jep j = new Jep();
        System.out.println("hello3");
        j.runScript("test.py");
        TestInterface pyInt = j.getValue("RocketInterface", TestInterface.class);
        TestInterface i = (TestInterface) Proxy.newProxyInstance(
                SimulationListenerInvocationHandler.class.getClassLoader(),
                new Class[]{TestInterface.class}, new SimulationListenerInvocationHandler());
//       Object i = j.getValue("RocketInterface", Object.class);
        System.out.println(i.getName());
        System.out.println(i.getRandomNumber());
    }
}
