#!/bin/python
import LSM9DS0

imu = LSM9DS0.LSM9DS0()
imu.set_mag_rate(LSM9DS0.LSM9DS0_MAGDATARATE_100HZ)
imu.set_mag_range(LSM9DS0.LSM9DS0_MAGGAIN_4GAUSS)
imu.set_accel_rate(LSM9DS0.LSM9DS0_ACCELDATARATE_400HZ)
imu.set_accel_range(LSM9DS0.LSM9DS0_ACCELRANGE_16G)
imu.set_gyro_range(LSM9DS0.LSM9DS0_GYROSCALE_500DPS)

while True:
    print(('{},'*8+'{}').format(*(imu.readGyro() + imu.readAccel() + imu.readMag())))
