#!/bin/bash

echo none | sudo tee /sys/class/leds/led0/trigger

while true; do
    if ping -w 3 -c 2 192.168.43.1; then
        echo 0 | sudo tee /sys/class/leds/led0/brightness
    else
        echo 1 | sudo tee /sys/class/leds/led0/brightness
    fi
done
