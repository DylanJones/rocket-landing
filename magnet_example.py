#!/bin/python
import logging
logging.getLogger().setLevel(logging.DEBUG)
import ins

ins.calibrate_base()

while True:
    s = ""
    s += "Rotation: [" + str(ins.get_rotation()[0])[:6] + ",  " + str(ins.get_rotation()[1])[:6] + "]       "

    print(s, end="\r")
