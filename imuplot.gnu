#!/usr/bin/env gnuplot
set datafile separator ","
plot "failed_launch.csv" using 1:2 with lines title "gx","failed_launch.csv" using 1:3 with lines title "gy","failed_launch.csv" using 1:4 with lines title "gz"
pause mouse close
plot "failed_launch.csv" using 1:5 with lines title "ax","failed_launch.csv" using 1:6 with lines title "ay","failed_launch.csv" using 1:7 with lines title "az"
pause mouse close
plot "failed_launch.csv" using 1:8 with lines title "mx","failed_launch.csv" using 1:9 with lines title "my","failed_launch.csv" using 1:10 with lines title "mz"
pause mouse close
