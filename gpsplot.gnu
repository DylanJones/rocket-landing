#!/usr/bin/env gnuplot
# set ticslevel 0
# set zrange[0:*]
set view 40,40
set datafile separator ','
# set termoption dashed
# unset key
set palette defined (0 'red',1 'green',2 'blue')

splot 'failed_launch.csv' using 11:12:13:0 with lines linecolor palette
#plot 'gps_data' using 0:1 with lines title 'x','gps_data' using 0:2 with lines title 'y','gps_data' using 0:3 with lines title 'z'
# plot 'gps_data' using 3 with lines title 'z'
pause mouse close
