#!/bin/python

import ins

ins.calibrate_base()

while True:
    s = "Altitude (m): "
    s += str(ins.get_altitude())

    print(s, end="\r")
