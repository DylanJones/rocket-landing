import LSM9DS0
import baro
import time
from mtk3339 import mt3339 # this is only for sending commands
import serial
import pynmea2

gps = mt3339('/dev/ttyAMA0')
gps.set_fix_update_rate(1000)
gps.set_nmea_update_rate(1000)
gps.set_baudrate(9600)

imu = LSM9DS0.LSM9DS0()

imu.set_accel_range(LSM9DS0.LSM9DS0_ACCELRANGE_2G)
imu.set_gyro_range(LSM9DS0.LSM9DS0_GYROSCALE_245DPS)

# ser = serial.Serial("/dev/ttyAMA0", 9600)

while True:
    print(f"Temperature: {baro.temperature()}")
    print(f"Altitude: {baro.altitude()}")

    print(f"Accel: {imu.readAccel()}")
    print(f"Magnetic field: {imu.readMag()}")
    print(f"Gyro: {imu.readGyro()}")

    print(f"Lat/Lon/Alt: {gps.lat}/{gps.lon}/{gps.alt}")
    print()

    time.sleep(1)
