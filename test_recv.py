#!/usr/bin/env python3
import serial
import os
import time
import RPi.GPIO as gpio

led_pin = 23

# ensure serial port exists
if not os.path.exists("/dev/ttySOFT0"):
    os.system("sudo insmod /home/pi/soft_uart.ko gpio_tx=15 gpio_rx=14")

ser = serial.Serial("/dev/ttySOFT0", 9600)
gpio.setmode(gpio.BCM)
gpio.setup(led_pin, gpio.OUT)

# disable trigger
os.system("echo none | sudo tee /sys/class/leds/led0/trigger")
# led off
os.system("echo 1 | sudo tee /sys/class/leds/led0/brightness")

while True:
    x = ser.read()
    if x == b'x':
        print("recv x")
        # gpio.output(led_pin, gpio.HIGH)
        os.system("echo 0 | sudo tee /sys/class/leds/led0/brightness")
        time.sleep(0.5)
        # gpio.output(led_pin, gpio.LOW)
        os.system("echo 1 | sudo tee /sys/class/leds/led0/brightness")
        time.sleep(0.5)
