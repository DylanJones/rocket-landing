import LSM9DS0
import baro
import time
from mtk3339 import mt3339 # this is only for sending commands
import serial
import pynmea2
from time import time, sleep
from threading import Thread
from math import atan2, atan, sqrt, cos, sin
import logging


## OPTIONS ##
calibrate_time = 25
calibrate_delay = 0.1
G_SCALAR = 9.81


## Variables ##
location_rel = [0, 0, 0]
logger = logging.getLogger("INS")

velocity = [0, 0, 0]

BASE_ALT = 0
BASE_G = 0
BASE_MAG = [0, 0, 0]
BASE_ROT = [0, 0]
    
## INTEGRATION ##
timer_interval = 0.0075
correction_timer_interval = 1

## Methods ##


def on_startup():
    global imu, gps, last_acc, last_rot
    ## Initial Setup ##
    gps = mt3339('/dev/ttyAMA0')
    gps.set_fix_update_rate(1000)
    gps.set_nmea_update_rate(1000)
    gps.set_baudrate(9600)

    imu = LSM9DS0.LSM9DS0()

    imu.set_accel_range(LSM9DS0.LSM9DS0_ACCELRANGE_8G)
    imu.set_gyro_range(LSM9DS0.LSM9DS0_GYROSCALE_245DPS)

    last_acc = time()
    last_rot = time()

    # Calibrate INS
    logger.info("Calibrating INS...")
    calibrate_base()
    logger.info("Calibration complete.")

    Thread(target=update_acc,daemon=True).start()


def calibrate_base():
    global BASE_ALT, BASE_G, BASE_ROT, BASE_MAG
    logger.info("Calibrating base altitude...")
    alts = []
    gs = []
    for x in range(calibrate_time):
        alts.append(baro.altitude())
        sleep(calibrate_delay)
        #gs.append(amu.readAccel())
    BASE_ALT = sum(alts) / float(len(alts))
    logger.info("Successfully calibrated base altitude.")

    logger.info("Calibrating magnetometer rotation...")
    rots_X = []
    rots_Y = []
    rots_Z = []
    for x in range(calibrate_time):
        rot = imu.readMag()
        rots_X.append(rot[0])
        rots_Y.append(rot[1])
        rots_Z.append(rot[2])
        sleep(calibrate_delay)
    BASE_MAG = [sum(rots_X) / float(calibrate_time), sum(rots_Y) / float(calibrate_time), sum(rots_Z) / float(calibrate_time)]
    BASE_ROT = mag_to_rot(BASE_MAG)
    logger.info("Successfully calibrated magnetometer rotation.")


def get_location():
    global location_rel
    return location_rel[:]

def get_altitude():
    return baro.altitude() - BASE_ALT

def get_altitude_absolute():
    return baro.altitude()

def get_rotation():
    return rot_difference(mag_to_rot(imu.readMag()), BASE_ROT)

# D E P R E C A T E D #
def get_acc_scaled():
    out = imu.readAccel()
    for i in out:
        i *= G_SCALAR
    return out


def mag_to_rot(mag):
    rot_xz = atan2(mag[0], mag[2])
    rot_y = atan(mag[1] / distance(mag[0], mag[2]))
    return [rot_xz, rot_y]

def rot_difference(rot_1, rot_2):
    return [rot_1[0] - rot_2[0], rot_1[1] - rot_2[1]]

def distance(x, y):
    return sqrt(x * x + y * y)

def rotation_correction(vector):
    angle = component_to_angle(vector)

    angle[0] = rot_difference(angle[0], get_rotation())
    return angle_to_component(angle)

def component_to_angle(vector):
    return [mag_to_rot(vector), distance(vector[0], distance(vector[1], vector[2]))]

def angle_to_component(vector):
    return [cos(vector[0][0]) * vector[1], sin(vector[0][1]) * vector[1], sin(vector[0][0]) * vector[1]]



def update_acc():
    global last_acc, timer_interval, location_rel, velocity
    while True:
        this_acc = imu.readAccel()
        t = time()
        dt = t - last_acc
        this_acc = rotation_correction(this_acc)
        #print("%.2f"%dt)
        for i in range(3):
            if this_acc[i] > 0.1:
                velocity[i] += this_acc[i] * dt
                location_rel[i] += velocity[i] * dt
        last_acc = t
        sleep(timer_interval)
        #print("Location : " + str(location_rel))
        #print("Velocity : " + str(velocity))


def update_rot():
    pass

def run_correction():
    global correction_timer_interval, location_rel, velocity
    while True:
        location_rel[1] =0 

if __name__ == "__main__":
    calibrate_base()
    while True:
        #print(imu.readGyro())
        print(get_rotation())
        sleep(1)



on_startup()
