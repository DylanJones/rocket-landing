#!/bin/python

import random

WIDTH = 94
TRIALS = 26
MUTATION = 0.3
GENERATIONS = 4000
POP = 500
LOW_CUTOFF = 0.000001
MAX_T = 200

display = ['']
disp = False
CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def run_trial(width, start, goal, gene, i=0):
    global LOW_CUTOFF, MAX_T, disp, display
    if gene[0] == 0:
        # U R   B A D
        return 10 ** 120
    MAX_DIST = abs(start-goal)
    t = 0
    score = 0
    location = start
    velocity = 0
    # pid
    integral = 0
    prev_error = goal - start
    result = 200
    while ( abs(velocity) > LOW_CUTOFF or abs(location - goal) > 0.1) and t < MAX_T:
        t += 1
        if disp:
            if 0 <= location + 0.5 < WIDTH:
                if display[t][int(location+0.5)] == " ":
                    display[t][int(location+0.5)] = CHARS[i]
                else:
                    display[t][int(location)] = "#"
            if t < len(display) and 0 <= goal < WIDTH and display[t][goal] == ' ':
                display[t][goal] = "|"
        error = goal - location
        integral += error
        derivative = error - prev_error
        prev_error = error
        result = gene[0] * error + gene[1] * derivative + gene[2] * integral
        if result == 0:
            break
        acceleration = min(abs(result), 1) * abs(result)/result
        #print(f'e:{error} pe:{prev_err} d:{derivative} a:{acceleration} l:{location}')
        velocity += acceleration
        #velocity *= 0.95
        location += velocity
        score += abs(error)
        if abs(location - goal) > MAX_DIST:
            # U R B A D
            return 10 ** 100
    return score

## D E P R E C A T E D ##
def action(power, t):
    power = int(power)
    act = power / abs(power)
    p = 20 - power + 1
    if t % p == 0:
        return act
    return 0

def test(gene, trials):
    global WIDTH, disp
    score = 0
    i = 0
    for trial in trials:
        score += run_trial(WIDTH, trial[0], trial[1], gene, i)
        i += 1
    displayPrint()
    if disp:
        print(f"Score: {score}")
    return score

def score(pool):
    global TRIALS, WIDTH
    #trials = [[random.randint(0, WIDTH),WIDTH//2] for i in range(TRIALS)]
    trials = [[(WIDTH / TRIALS) * a, WIDTH//2] for a in range(TRIALS)]
    scores = []
    for gene in pool:
        scores.append([test(gene,trials), gene])
    out = sorted(scores, key=lambda k: k[0])[:150]
    return [index[1] for index in out]

def repopulate(pool, step):
    global POP
    output = [pool[0][:]]
    # add one random boi
    for x in range(10):
        output.append([random.random(), random.random(), 0])
    for x in range(POP - 1):
        output.append(combine(random.choice(pool)[:], random.choice(pool)[:], step))
    return output

def combine(gene1, gene2, step):
    for i in range(3):
        r = random.randint(0, 2)
        if r == 0:
            gene1[i] = (gene1[i] + gene2[i]) / 2
        if r == 1:
            gene1[i] = gene2[i]
    if random.random() < MUTATION:
        gene1[random.randint(0,1)] += (random.random() - 0.5) * 0.2
    for i in range(len(gene1)):
        gene1[i] = max(min(gene1[i], 1), 0)
    return gene1

def displayReset():
    global display, WIDTH, MAX_T
    display = [[" "] * WIDTH for a in range(MAX_T+1)]

def displayPrint():
    if not disp:
        return
    print("-" * WIDTH)
    for l in display:
        s = "".join(l)
        if s.strip() != "":
            print(s)
    displayReset()

if __name__ == "__main__":
    displayReset()
    ## do things
    pool = [ [random.random(), random.random(), 0] for i in range(POP)]
    ## train direct
    best = pool[0]
    for x in range(GENERATIONS):
        print(f" Currently simulating generation {x}...",end="\r")
        out = score(pool)
        if out[0] != best:
            disp = True
            best = out[0]
            score([best])
            print(f"Best candidate for generation {x}: {best}")
            disp = False
        pool = repopulate(out, 1)
    
    pool = score(pool)
    disp = True
    pool = pool[::-1]
    score(pool)
    print(f"Best candidate:{pool[0]}")
    test(pool[0], [[0, 90]])
